__author__ = 'pr3mar'
import json, sys, time

def printToFile(fname, data):
    sys.stdout = open(fname, "w")
    for item in data:
        print json.dumps(item)
    sys.stdout = sys.__stdout__

edinburgh = []
reviewCount = 0
businessID = set()
reviews = []
tips = []
checkins = []
users = []
userIds = set()
t = time.time()

with open("../../original/yelp_academic_dataset_business.json") as file:
    for line in file:
        js = json.loads(line)
        if js["city"] == "Edinburgh":
            edinburgh.append(js)
            reviewCount += js["review_count"]
            businessID.add(js["business_id"])

with open("../../original/yelp_academic_dataset_review.json") as file:
    for line in file:
        js = json.loads(line)
        if js["business_id"] in businessID:
            reviews.append(js)
            userIds.add(js["user_id"])

with open("../../original/yelp_academic_dataset_tip.json") as file:
    for line in file:
        js = json.loads(line)
        if js["business_id"] in businessID:
            tips.append(js)
            userIds.add(js["user_id"])

with open("../../original/yelp_academic_dataset_checkin.json") as file:
    for line in file:
        js = json.loads(line)
        if js["business_id"] in businessID:
            checkins.append(js)

with open("../../original/yelp_academic_dataset_user.json") as file:
    for line in file:
        js = json.loads(line)
        if js["user_id"] in userIds:
            users.append(js)

elapsed = time.time() - t
printToFile("../data/exp/edinburgh_business.json", edinburgh)
printToFile("../data/exp/edinburgh_reviews.json", reviews)
printToFile("../data/exp/edinburgh_tips.json", tips)
printToFile("../data/exp/edinburgh_checkins.json", checkins)
#printToFile("../data/tiny/edinburgh_user_ids.json", userIds)
printToFile("../data/exp/edinburgh_users.json", users)


print "time:", elapsed
print "businesses in edinburgh: ", len(edinburgh)
print "review count: ", reviewCount
print "reviews: ", len(reviews)
print "tips: ", len(tips)
print "checkins: ", len(checkins)
print "users: ", len(userIds)
