__author__ = 'pr3mar'
import sys, json

def threshold(value, tau):
    if value >= tau:
        return 1
    else:
        return 0
tau = 2
reviews = [
    ["business_id", "user_id", "stars", "funny", "useful", "cool", "neg", "pos", "neutral", "label"],
    ["s", "s", "c", "d", "d", "d", "c", "c", "c", "d"],
    ["m", "m", "", "", "", "", "", "", "", "c"]
]
for i in reviews:
    print len(i)
revs = {}
with open("../data/exp/edinburgh_reviews.json") as file:
    for line in file:
        js = json.loads(line)
        tmp = []
        if js["business_id"] not in revs:
            revs[js["business_id"]] = {}
        revs[js["business_id"]][js["user_id"]] = {}
        revs[js["business_id"]][js["user_id"]]["date"] = js["date"]
        revs[js["business_id"]][js["user_id"]]["stars"] = js["stars"]
        revs[js["business_id"]][js["user_id"]]["funny"] = threshold(js["votes"]["funny"], tau)
        revs[js["business_id"]][js["user_id"]]["useful"] = threshold(js["votes"]["useful"], tau)
        revs[js["business_id"]][js["user_id"]]["cool"] = threshold(js["votes"]["cool"], tau)
# print revs
with open("../data/sentimental_analysis_responses") as file:
    for line in file:
        tmp = []
        try:
            review = json.loads(line)
            tmp.append(review["business_id"])
            tmp.append(review["user_id"])
            tmp.append(revs[review["business_id"]][review["user_id"]]["stars"])
            tmp.append(revs[review["business_id"]][review["user_id"]]["funny"])
            tmp.append(revs[review["business_id"]][review["user_id"]]["useful"])
            tmp.append(revs[review["business_id"]][review["user_id"]]["cool"])
            tmp.append(review["probability"]["neg"])
            tmp.append(review["probability"]["pos"])
            tmp.append(review["probability"]["neutral"])
            tmp.append(review["label"])
            reviews.append(tmp)
        except:
            print line
sys.stdout = open("../data/tab/sentimental_analysis_all.tab", "w")
for line in reviews:
    for att in line:
        if isinstance(att, (int, float)):
            sys.stdout.write(str(att))
        else:
            sys.stdout.write(att.encode("utf-8"))
        sys.stdout.write("\t")
    print
sys.stdout = sys.__stdout__