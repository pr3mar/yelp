__author__ = 'pr3mar'

import json, sys, time

business = [
    ["id", "name", "address", "city", "latitude", "longitude", "stars", "no_categories", "no_attributes", "open", "category"],
    ["s", "s", "s", "s", "c", "c", "c", "d", "d", "d", "d"],
    ["m", "m", "m", "m", "", "", "", "", "", "", "c"]
]

with open("../data/exp/categories.json") as file:
    categories = json.loads(file.read())
cats = []
for key, value in categories.items():
    business[0].append(key)
    business[1].append("d")
    business[2].append("")
    cats.append(0)
business[0].append("other")
business[1].append("d")
business[2].append("")
cats.append(0)
with open("../data/exp/edinburgh_business.json") as file:
    for line in file:
        js = json.loads(line)
        tmp = []
        category = cats[:]
        tmp.append(js["business_id"])
        tmp.append(' '.join(js["name"].split()))
        tmp.append(' '.join(js["full_address"].split()))
        tmp.append(js["city"])
        tmp.append(js["longitude"])
        tmp.append(js["latitude"])
        tmp.append(js["stars"])
        tmp.append(len(js["categories"]))
        tmp.append(len(js["attributes"]))
        tmp.append(js["open"])
        majorCat = False
        for item in js["categories"]:
            i = 0
            for key, value in categories.items():
                if item == key or item in value["subcategories"]:
                    category[i] = 1
                    if majorCat == False:
                        majorCat = True
                        tmp.append(i)
                i += 1
        if not majorCat:
            category[-1] = 1
            tmp.append(len(category) - 1)
        for c in category:
            tmp.append(c)
        # print tmp
        business.append(tmp)

sys.stdout = open("../data/tab/business_experiment.tab", "w")
for i in business:
    for att in i:
        if isinstance(att, (int, float)):
            sys.stdout.write(str(att))
        else:
            sys.stdout.write(att.encode("utf-8"))
        sys.stdout.write("\t")
    print
sys.stdout = sys.__stdout__