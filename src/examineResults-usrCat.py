from __future__ import division
import numpy as np, time, sys, json
import matplotlib.pyplot as plt
from sklearn.metrics import roc_auc_score
posBus = 0; negBus = 0;

def check(val):
    # val = int(round(val))
    global posBus, negBus, neutral, nan
    if(val >= 2.0):
        posBus += 1
        return 3
    else:
        negBus += 1
        return 1

categories = ["Bars", "Fashion", "Shopping", "Food", "Beauty & Spas",
              "Local Services", "Arts & Entertainment", "Active Life",
              "Public Services & Government", "Nightlife", "Hotels & Travel",
              "Local Flavor", "Event Planning & Services", "Restaurants", "other"]
printUserCategoryPrediction = True
categoryPredict = False

json_users = {}
users = []
with open("../data/draw/users.js") as file:
    json_users = json.load(file)
    for key in json_users:
        users.append(key.encode("ascii"))
begin = time.time()
original_normalized = np.loadtxt("../data/nmf/user-cat/uc_10_500-2015-06-02-12-34-20.txt")
approximation = np.loadtxt("../data/nmf/user-cat/app_uc_10_500_2015-06-02-12-34-20.txt")
print "time loading = ", time.time() - begin

for row in range(approximation.shape[0]):
    tmp = []
    for column in range(approximation.shape[1]):
        tmp.append((approximation[row, column], categories[column]))
        approximation[row, column] = check(approximation[row, column])
    tmp.sort(reverse=True)
    json_users[users[row]]["predictedCategories"] = [i[1] for i in tmp[:5]]

if printUserCategoryPrediction:
    sys.stdout = open("../data/draw/user_withCats.js","w")
    print "users = {"
    for key, value in json_users.items():
        print '\"' + key + '\": ' + json.dumps(value) + ", "
    print "}"
    sys.stdout = sys.__stdout__

if categoryPredict:
    print posBus, negBus
    vals = [[], []]
    indices = np.nonzero(original_normalized)
    rez = np.where(approximation[indices] == original_normalized[indices])
    print len(rez[0])
    print len(indices[0])
    print len(rez[0])/len(indices[0])
    for column in range(approximation.shape[1]):
        locPos = 0; locNeg = 0; locNeutral = 0; locNan = 0
        for row in range(approximation.shape[0]):
            val = approximation[row, column]
            if(val == 3):
                locPos += 1
            else:
                locNeg += 1
        vals[0].append((locPos, categories[column]))
        vals[1].append((locNeg, categories[column]))

    percents = [[], []]
    for i in vals[0]:
        percents[0].append((i[0]/posBus, i[1]))
    for i in vals[1]:
        percents[1].append((i[0]/negBus, i[1]))

    for i in percents:
        i.sort(reverse=True)
        k = 0
        for j in i:
            print (j[0] * 100, j[1]),
            k += 1
            if k == 5:
                break
        print