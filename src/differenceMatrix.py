__author__ = 'pr3mar'
import numpy as np, datetime, time
from scipy.stats import norm
import matplotlib.pyplot as plt

original = np.loadtxt("../data/nmf/user-bus/notNormal_sampled_ub_10_500_2015-05-31-18-15-08.txt")
approximation = np.loadtxt("../data/nmf/user-bus/normal_sampled_app_ub_10_500_2015-05-31-18-15-14.txt")
date = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d-%H-%M-%S')

indices = np.nonzero(original)
meanOrg = np.mean(original[indices])
stdOrg = np.std(original[indices])
meanApr = np.mean(approximation[indices])
stdApr = np.std(approximation[indices])
diff = (approximation[indices] - original[indices]) ** 2

print meanOrg, stdOrg
print meanApr, stdApr, "\n"

x = np.linspace(-15,15,300)
orgNorm = norm.fit(original[indices])
aprNorm = norm.fit(approximation[indices])
diffNorm = norm.fit(diff)

print orgNorm[0], orgNorm[1]
print aprNorm[0], aprNorm[1]
print diffNorm[0], diffNorm[1]

org_fitted = norm.pdf(x,loc=orgNorm[0],scale=orgNorm[1])
apr_fitted = norm.pdf(x,loc=aprNorm[0],scale=aprNorm[1])
diff_fitted = norm.pdf(x,loc=diffNorm[0],scale=diffNorm[1])
normal = norm.pdf(x)
plt.figure(figsize=plt.figaspect(0.5), dpi=100)
plt.title('User : Business')
plt.plot(x, normal, 'r-', label="normal distribution")
plt.plot(x, org_fitted, 'b-', label="Original data")
plt.plot(x, apr_fitted, 'y-', label="PGDNMF approximated data")
plt.plot(x, diff_fitted, 'g-', label="diff(Approximated, Original)")
plt.legend()
plt.savefig("../data/imgs/ub_" + date + ".png", dpi=1000)
plt.show()