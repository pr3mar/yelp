categories = {
    "Bars": {"count": 1, "subcategories": ["Pubs"]},
    "Fashion": {"count": 2, "subcategories": ["Accessories", "Women's Clothing"]},
    "Shopping": {
        "count": 6,
        "subcategories": ["Books, Mags, Music & Video", "Accessories", "Arts & Crafts", "Flowers & Gifts", "Fashion", "Women's Clothing"]
    },
    "Food": {
        "count": 5,
        "subcategories": ["Grocery", "Coffee & Tea", "Beer, Wine & Spirits", "Bakeries", "Specialty Food"]
    },
    "Beauty & Spas": {"count": 0, "subcategories": []},
    "Local Services": {"count": 0, "subcategories": []},
    "Arts & Entertainment": {"count": 0, "subcategories": []},
    "Active Life": {"count": 0, "subcategories": []},
    "Public Services & Government": {"count": 0, "subcategories": []},
    "Nightlife": {"count": 2, "subcategories": ["Bars", "Pubs"]},
    "Local Flavor": {"count": 0, "subcategories": []},
    "Event Planning & Services": {"count": 1, "subcategories": ["Hotels"]},
    "Hotels & Travel": {"count": 1, "subcategories": ["Hotels"]},
    "Restaurants": {
        "count": 9,
        "subcategories": ["Delis", "Chinese", "British", "Sandwiches", "Indian", "Italian", "Cafes", "Gastropubs", "Fast Food"]
    }
}
