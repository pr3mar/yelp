__author__ = 'pr3mar'
import requests, json, sys, unirest

url = "http://text-processing.com/api/sentiment/"
# datum = "text=a good movie"
# response = requests.post(url, data=datum)
# print json.loads(response.text)["label"]
missing = set()
with open("../data/missing_responses.txt") as file:
    for line in file:
        missing.add(line.rstrip())
print len(missing)
countMissing = 0
ordinaryCount = 0
sys.stdout = open("../data/responses_limited_api_get_missing", "w")
with open("../data/exp/edinburgh_reviews.json") as file:
    for line in file:
        ordinaryCount += 1
        review = json.loads(line)
        if(review["review_id"] in missing):
            try:
                response = unirest.post("https://japerk-text-processing.p.mashape.com/sentiment/",
                    headers={
                        "X-Mashape-Key": "t7hURno0vcmshbOInoJRGTDjkfXTp1sBe1gjsnBxx3SJ6Zohpt",
                        "Content-Type": "application/x-www-form-urlencoded",
                        "Accept": "application/json"
                    },
                    params={
                        "language": "english",
                        "text": review["text"].encode("utf-8")
                    }
                )
                if (response.code == 200):
                    resp = response.body
                    resp["business_id"] = review["business_id"]
                    resp["user_id"] = review["user_id"]
                    resp["review_id"] = review["review_id"]
                    resp["stars"] = review["stars"]
                    resp["funny"] = review["votes"]["funny"]
                    resp["cool"] = review["votes"]["cool"]
                    resp["useful"] = review["votes"]["useful"]
                    print json.dumps(resp)
                    countMissing += 1
                else:
                    print "[error:" + response.code + "] not included: " + review["review_id"]
            except:
                print "[error] not included: " + review["review_id"]
            sys.stdout.flush()
sys.stdout = sys.__stdout__
print ordinaryCount, countMissing