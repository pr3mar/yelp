#TODO get the best businesses for each user (top 5) based on rating and enter the ids in json object
#TODO add data about categories, so can be colored/viewed by category (another clustering method)
from __future__ import division
import numpy as np, time, sys, json
import matplotlib.pyplot as plt
from scipy.stats import norm
from sklearn.metrics import roc_auc_score
posBus = 0; negBus = 0; neutralBus = 0
# categories = ["Fashion", "Shopping", "Food", "Beauty & Spas", "Local Services", "Arts & Entertainment",
#               "Active Life", "Public Services & Government", "Nightlife", "Hotels & Travel",
#               "Local Flavor", "Event Planning & Services", "Restaurants", "other"]
with open("../data/exp/categories.json") as file:
    categories = json.loads(file.read())
def check(val):
    val = int(round(val))
    global posBus, negBus, neutralBus, nan
    if(val >= 3):
        posBus += 1
        return 3
    elif(val >= 1 and val < 3):
        neutralBus += 1
        return 2
    elif val < 1:
        negBus += 1
        return 1
def popularity(data):
    pop =  0.2*data["votes"]["funny"]/allFunny + 0.3*data["votes"]["cool"]/allCool + 0.5*data["votes"]["useful"]/allUseful
    pop += ( (data["average_stars"]/5) * (data["review_count"] + 1))/1000 + data["fans"]/(len(data["friends"]) + 1)
    allComp = 0
    try:
        for key, data in data["compliments"].items():
            allComp += data
        for key, data in data["compliments"].items():
            pop += data/allComp
    except:
        pass
    try:
        pop += 0.3 * len(data["elite"])
    except:
        pass
    return pop

def getCategory(business):
    cats = []
    for key, value in categories.items():
        for cat in business:
            if(cat in value["subcategories"] or cat == key):
                cats.append(key)
    cats = set(cats)
    if(len(cats) == 0):
        cats.add("other")
    # return list(categories[i] for i in np.nonzero(np.array(business).astype(int))[0])
    return list(cats)

def getLabBusiness(pos, neg, neu):
    if(pos > neg and pos > neu):
        return 'positive'#'<span style="color:green; font-weight: bold"; >positive</span>.<br> I think you will like this place!'
    elif(neu >= neg and neu >= pos):
        return 'neutral'#'<span style="color:yellowgreen; font-weight: bold">neutral</span>.<br> I am not sure if you will like this store, check it out!'
    elif(neg > pos and neg > neu):
        return 'negative'#'<span style="color:red; font-weight: bold">negative</span>.<br> Uuumm, I don\'t think  you will like this place :/'

showImgs = False
printBusinesses = False
printUsers = True
business = []
json_business = {}
users = []
json_users = {}
# load files
allVotes = 0
allFunny = 0
allUseful = 0
allCool = 0
begin = time.time()

with open("../data/exp/edinburgh_business.json") as file:
    for line in file:
        data = json.loads(line)
        del data["type"];del data["neighborhoods"];del data["open"];del data["state"];del data["hours"];del data["attributes"]
        json_business[data["business_id"]] = data
        business.append([data["business_id"]])

with open("../data/exp/edinburgh_users.json") as file:
    for line in file:
        data = json.loads(line)
        del data["yelping_since"];del data["type"]
        allFunny += data["votes"]["funny"];allCool += data["votes"]["cool"];allUseful += data["votes"]["useful"]
        allVotes += data["votes"]["funny"] + data["votes"]["cool"] + data["votes"]["useful"]
        json_users[data["user_id"]] = data
        users.append(data["user_id"].encode("ascii"))
with open("../data/tab/business_clustered_15_clusters.tab") as file:
    i = 0
    for line in file:
        if i > 2:
            data = line.split("\t")
            key = data[-1].rstrip()
            json_business[key]["cluster"] = data[18]
            json_business[key]["majorCategories"] = getCategory(json_business[key]["categories"])#getCategory(data[1:15])
        i += 1
#load matrices
original_NotNormalized = np.loadtxt("../data/nmf/user-bus/notNormal_ub_10_500_2015-05-31-18-14-10.txt")
approximation = np.loadtxt("../data/nmf/user-bus/notNormal_app_ub_10_500_2015-05-31-18-14-10.txt")
print "time loading = ", time.time() - begin

indices = np.nonzero(original_NotNormalized)
meanOrg = np.mean(original_NotNormalized[indices])
stdOrg = np.std(original_NotNormalized[indices])
meanApr = np.mean(approximation[indices])
stdApr = np.std(approximation[indices])

begMatrix = time.time()

# users/ normalization
for row in range(approximation.shape[0]):
    tmp = []
    for column in range(approximation.shape[1]):
        tmp.append((approximation[row, column], business[column][0]))
        approximation[row, column] = check(approximation[row, column])
    tmp.sort(reverse=True)
    json_users[users[row]]["predictions"] = [i[1] for i in tmp[:5]]
    data = json_users[users[row]]
    json_users[users[row]]["popularity"] = popularity(data)
print "do the matrix and sort = ", time.time() - begMatrix

print posBus, neutralBus, negBus

# percentage of matching
rez = np.where(np.round(approximation[indices]) == np.round(original_NotNormalized[indices]))
print len(rez[0])
print len(indices[0])
print len(rez[0])/len(indices[0])

# businesses:
if(printBusinesses):
    for column in range(approximation.shape[1]):
        locPos = 0; locNeg = 0; locNeutral = 0; locNan = 0
        for row in range(approximation.shape[0]):
            val = approximation[row, column]
            if(val == 3):
                locPos += 1
            elif(val == 2):
                locNeutral += 1
            elif(val == 1):
                locNeg += 1
        business[column].append((locPos/posBus, locNeutral/neutralBus, locNeg/negBus))
        json_business[business[column][0]]["positive"] = locPos/posBus
        json_business[business[column][0]]["negative"] = locNeg/negBus
        json_business[business[column][0]]["neutral"] = locNeutral/neutralBus
        json_business[business[column][0]]["label"] = getLabBusiness(locPos/posBus, locNeutral/neutralBus,locNeg/negBus)
    #generate json for maps
    sys.stdout = open("web/business_.js", "w")
    print "business = {"
    for key, value in json_business.items():
        print '\"' + key + '\": ' + json.dumps(value) + ", "
    print "}"
    sys.stdout = sys.__stdout__

if(printUsers):
    sys.stdout = open("../data/draw/users.js", "w")
    print "users = {"
    for key, value in json_users.items():
        print '\"' + key + '\": ' + json.dumps(value) + ", "
    print "}"
    sys.stdout = sys.__stdout__

if(showImgs):
    W = np.loadtxt("../data/nmf/user-bus/notNormal_pgdnmf_w_an_ub_matrix2_10_500_all.txt")
    Hs = np.loadtxt("../data/nmf/user-bus/notNormal_pgdnmf_h_an_ub_matrix2_10_500_all.txt")
    plt.figure(figsize=(5.0, 5.0))
    plt.imshow(original_NotNormalized)
    plt.title("Original")

    plt.figure(figsize=(5.0, 5.0))
    plt.title("Approximation")
    plt.imshow(approximation)

    plt.figure()
    plt.title("Clusters")
    plt.pcolor(W)
    plt.figure()
    plt.title("Features")
    plt.pcolor(Hs)

    plt.show()