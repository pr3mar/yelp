__author__ = 'pr3mar'
import json, sys, time
categories = set()
eachBusiness = []

with open("../data/exp/edinburgh_business.json") as file:
    for line in file:
        js = json.loads(line)
        for item in js["categories"]:
            categories.add(item)
        eachBusiness.append(js["categories"])

print len(categories)
sys.stdout = open("../data/categories", "w")
for i in categories:
    print i
sys.stdout = sys.__stdout__

eachCategory = {}
for i in categories:
    eachCategory[i] = {}
    for j in eachBusiness:
        if i in j:
            for k in j:
                if k in eachCategory[i]:
                    eachCategory[i][k] += 1
                else:
                    eachCategory[i][k] = 1
majorCategories = {}
businessCounter = 0
for key, value in eachCategory.items():
    if(value[key] > 50):
        businessCounter += value[key]
        major = True
        for i, j in value.items():
            if(j == value[key] and i != key):
                major = False
                if i in majorCategories:
                    # majorCategories[i] += 1
                    majorCategories[i]["subcategories"].append(key)
                    majorCategories[i]["count"] += 1
                else:
                    # majorCategories[i] = 1
                    majorCategories[i] = {"subcategories":[key], "count":1}
        if major and (key not in majorCategories):
            # majorCategories[key] = 1
            majorCategories[key] = {"subcategories":[], "count":0}
sys.stdout = open("../data/major_categories", "w")
for key, value in majorCategories.items():
    print key, value["count"]
    for v in value["subcategories"]:
        print "\t", v
sys.stdout = sys.__stdout__

sys.stdout = open("../data/exp/categories.json", "w")
print json.dumps(majorCategories)#, sort_keys=True, indent=4)