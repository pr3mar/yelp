__author__ = 'pr3mar'
import numpy as np
import json, sys, random as rnd, time, datetime
from scipy.stats import norm
import matplotlib.pyplot as plt
import scipy.sparse as sp
from sklearn.cross_validation import StratifiedShuffleSplit


def printMatrix(fname, matrix):
    sys.stdout = open(fname, "w")
    for i in matrix:
        for j in i:
            print j,
        print
    sys.stdout = sys.__stdout__

def dotp(args):
    """
        Dot product for both sparse and non-sparse matrices.
        :param args
            List of NumPy arrays and/or SciPY sparse matrices.
        :return
            Result of dot product. If any matrix in args was sparse, dot product is sparse.
    """
    if len(args) == 1:
        return args[0]
    indicator = [sp.isspmatrix(t) for t in args]
    if any(indicator):
        i = indicator.index(True)
        j = i+1
        if i <= len(args) - 2:
            # i is not last
            return dotp(args[:i] + [args[i].dot(args[j])] + args[j+1:])
        else:
            # i is last
            j = i - 1
            return dotp(args[:j] + [args[i].T.dot(args[j].T).T])
    else:
        # no matrix is sparse
        # find maximum eliminated dimension (TODO)
        return dotp([args[0].dot(args[1])]+args[2:])

def check(label):
    if label == "pos":
        return 3
    elif label == "neutral":
        return 2
    elif label == "neg":
        return 1
    else:
        return 0

def pgdnmf(X, rank, max_iter=100, tol=1e-5, eta=1e-2):
    """
        NMF using projected gradient descent.
        Minimize || X - WH ||_F, subject to W, H >= 0.


        :param X:
            Data matrix.
        :param rank
            Maximal rank of the model (W, H).
        :param max_iter
            Maximum number of iterations.

        :return
            Data model W, H.
    """
    m, n  = X.shape
    W     = np.random.rand(m, rank)
    H     = np.random.rand(rank, n)
    itr   = 0
    error = float("inf")

    known = X.nonzero()

    while error > tol and itr < max_iter:
        error = 0
        if itr % 10 == 0:
            sys.stdout.write("%s/%s\r" % (itr, max_iter))
            sys.stdout.flush()

        dw = np.zeros((m, rank))
        dh = np.zeros((rank, n))
        for i, j in zip(*known):
            for k in xrange(rank):
                dw[i, k]    = dw[i, k] + (X[i, j] - W[i, :].dot(H[:, j])) * H[k, j]
                dh[k, j]    = dh[k, j] + (X[i, j] - W[i, :].dot(H[:, j])) * W[i, k]
            error += (X[i, j] - W[i, :].dot(H[:, j]))**2

        W = W + eta * dw
        H = H + eta * dh
        W = W * (W > 0)
        H = H * (H > 0)
        itr += 1

    return W, H

def nmf(Xs, rank, max_iter=100, allow_negative=False):
    """
    Standard non-negative matrix factorization.
    :param Xs
        Data matrices.
    :param rank
        Factorization rank (r).
    :param max_iter.
        Maximum number of iterations.
    :param getobj
        Return vector of cost function values.
    :return:
        W: A common row clustering matrix.
        Hs: A list of data-source specific matrices.
    """
    m, Ns = Xs[0].shape[0], [x.shape[1] for x in Xs]
    W = np.random.rand(m, rank)
    Hs = [np.random.rand(rank, n) for n in Ns]

    for itr in range(max_iter):
        sys.stderr.write("%d/%d\r" % (itr, max_iter) )
        sys.stderr.flush()

        enum = dotp([Xs[0], Hs[0].T])
        denom = dotp([W, dotp([Hs[0], Hs[0].T])])
        for i in range(1, len(Hs)):
            enum += dotp([Xs[i], Hs[i].T])
            denom += dotp([W, dotp([Hs[i], Hs[i].T])])
        W = np.nan_to_num(W * enum/denom)

        for i in range(len(Hs)):
            enum = dotp([W.T, Xs[i]])
            denom = dotp([W.T, W, Hs[i]])
            Hs[i] = np.nan_to_num(Hs[i] * enum/denom)

    return W, Hs

sample = True
normal = True
date = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d-%H-%M-%S')
users = {}
business = {}
with open("../data/tab/edinburgh_users.tab") as file:
    i = 0
    for line in file:
        if i > 2:
            users[line.split("\t")[0]] = i - 3
        i += 1

with open("../data/tab/edinburgh_business.tab") as file:
    i = 0
    for line in file:
        if i > 2:
            business[line.split("\t")[0]] = i - 3
        i += 1

print len(users), len(business)
matrix = np.zeros((len(users), len(business)))

with open("../data/sentimental_analysis_responses") as file:
    for line in file:
        try:
            entry = json.loads(line)
            matrix[users[entry["user_id"]], business[entry["business_id"]]] = check(entry["label"])
        except:
            pass

# normalization
if(normal):
    fname = "normal_"
    for i in matrix:
        indices = np.nonzero(i)
        if(len(i[indices]) > 0):
            mean = np.mean(i[indices])
            std = np.std(i[indices])
            for j in indices:
                for k in j:
                    if(std > 0):
                        if (i[k] == mean):
                            i[k] = i[k]/std
                        else:
                            i[k] = (i[k] - mean)/std
else:
    fname = "notNormal_"
indices = np.nonzero(matrix)

# sampling
if(sample):
    fname += "sampled_"
    k = int(round(float(len(indices[0]))*0.3))
    sample = rnd.sample(indices[0], k)
    samp = [[], []]
    for k in sample:
        samp[0].append(indices[0][k])
        samp[1].append(indices[1][k])
    samp[0] = np.array(samp[0])
    samp[1] = np.array(samp[1])
    sample = samp
    saveVals = matrix[sample]
    matrix[sample] = 0
    indices = sample
    sys.stdout = open("../data/nmf/missing/indices.txt", "w")
    for i in indices:
        for j in i:
            print j,
        print
    sys.stdout = sys.__stdout__

begin = time.time()
param1 = 15
param2 = 100
[W, H] = pgdnmf(matrix, param1, param2)
approximatePGDNMF = W.dot(H)
print "time[pgdnmf] = ", time.time() - begin

begin = time.time()
#ub = user business, app = approximated
printMatrix("../data/nmf/user-bus/" + fname + "ub_"+ str(param1) + "_" + str(param2) + "_" + date + ".txt", matrix)
printMatrix("../data/nmf/user-bus/" + fname + "app_ub_"+ str(param1) + "_" + str(param2) + "_" + date + ".txt", approximatePGDNMF)
printMatrix("../data/nmf/user-bus/" + fname + "w_ub_"+ str(param1) + "_" + str(param2) + "_" + date + ".txt", W)
printMatrix("../data/nmf/user-bus/" + fname + "h_ub_"+ str(param1) + "_" + str(param2) + "_" + date + ".txt", H)
print "time[print] = ", time.time() - begin


diffPGDNMF = (approximatePGDNMF[indices] - matrix[indices]) ** 2


print "[pgdnmf] diff mean and std"
print np.mean(diffPGDNMF)
print np.std(diffPGDNMF)

x = np.linspace(-15,15,300)
orgNorm = norm.fit(matrix[indices])
aprNorm = norm.fit(approximatePGDNMF[indices])
diffNormPGDNMF = norm.fit(diffPGDNMF)

org_fitted = norm.pdf(x,loc=orgNorm[0],scale=orgNorm[1])
apr_fitted = norm.pdf(x,loc=aprNorm[0],scale=aprNorm[1])
diffPGDNMF_fitted = norm.pdf(x,loc=diffNormPGDNMF[0],scale=diffNormPGDNMF[1])
normal = norm.pdf(x)
plt.figure(figsize=plt.figaspect(0.5), dpi=100)
plt.title('PGDNMF differences all indices')
plt.plot(x, normal, 'b-', label="normal distribution")
plt.plot(x, org_fitted, 'r-', label="original data")
plt.plot(x, apr_fitted, 'y-', label="approximation")
plt.plot(x, diffPGDNMF_fitted, "g-", label="difference")
plt.legend()
plt.savefig("../data/imgs/ub_" + str(param1) + "_" + str(param2) + "_" + date + ".png", dpi=1000)
plt.show()

