__author__ = 'pr3mar'
import json, sys, time

def printToTabFile(fname, data, extra=None, discrete=True):
    sys.stdout = open(fname, "w")
    if extra != None:
        for item in extra:
            if isinstance(item, (int, float)):
                sys.stdout.write(str(item))
            else:
                sys.stdout.write(' '.join(item.split()).encode("utf-8"))
            sys.stdout.write("\t")
        print
        for item in extra:
            if discrete == True:
                sys.stdout.write("d".encode("utf-8"))
            else:
                sys.stdout.write("c".encode("utf-8"))
            sys.stdout.write("\t")
        print
    for item in data:
        for att in item:
            if isinstance(att, (int, float)):
                sys.stdout.write(str(att))
            else:
                sys.stdout.write(att.encode("utf-8"))
            sys.stdout.write("\t")
        print
    sys.stdout = sys.__stdout__

def printBusinesses(fname, business, categories):
    sys.stdout = open(fname, "w")
    for item1, item2 in zip(business, categories):
        for att in item1:
            if isinstance(att, (int, float)):
                sys.stdout.write(str(att))
            else:
                sys.stdout.write(att.encode("utf-8"))
            sys.stdout.write("\t")
        for att in item2:
            if isinstance(att, (int, float)):
                sys.stdout.write(str(att))
            else:
                sys.stdout.write(att.encode("utf-8"))
            sys.stdout.write("\t")
        print


def checkIfSet(value):
    if(value == 0):
        return 0
    else:
        return value

def threshold(value, tau):
    if value >= tau:
        return 1
    else:
        return 0

users = [
    ["id", "name", "date_joined", "review_count", "avg_stars", "no_fans", "no_friends"],
    ["s", "s", "s", "d", "c", "c", "c"],
    ["m", "m", "m", "", "", "", ""]
]
business = [
    ["id", "name", "address", "city", "latitude", "longitude", "stars", "no_categories", "no_attributes", "open"],
    ["s", "s", "s", "s", "d", "d", "c", "d", "d", "d"],
    ["m", "m", "m", "m", "", "", "", "", "", "c"]
]

reviews = [
    ["business_id", "user_id", "date-day", "date-month", "date-year", "stars", "funny", "useful", "cool", "text"],
    ["d", "s", "d", "d", "d", "d", "d", "d", "d", "s"],
    ["m", "m", "", "", "", "", "", "", "c", "m"]
]

tips = [
    ["business_id", "user_id", "date-day", "date-month", "date-year", "likes", "text"],
    ["d", "s", "d", "d", "d", "d", "s"],
    ["c", "m", "", "", "", "", "m"]
]

tau = 2

with open("../data/exp/edinburgh_users.json") as file:
    for line in file:
        js = json.loads(line)
        tmp = []
        tmp.append(js["user_id"])
        tmp.append(js["name"])
        tmp.append(js["yelping_since"])
        tmp.append(js["review_count"])
        tmp.append(js["average_stars"])
        tmp.append(js["fans"])
        tmp.append(len(js["friends"]))
        users.append(tmp)

categories = set()
attributes = set()
buss_ids = []

with open("../data/exp/edinburgh_business.json") as file:
    for line in file:
        js = json.loads(line)
        tmp = []
        tmp.append(js["business_id"])
        buss_ids.append(js["business_id"])
        tmp.append(' '.join(js["name"].split()))
        tmp.append(' '.join(js["full_address"].split()))
        tmp.append(js["city"])
        tmp.append(js["longitude"])
        tmp.append(js["latitude"])
        tmp.append(js["stars"])
        tmp.append(len(js["categories"]))
        tmp.append(len(js["attributes"]))
        tmp.append(js["open"])
        for item in js["categories"]:
            categories.add(item)
        for item in js["attributes"]:
            attributes.add(item)
        # print tmp
        business.append(tmp)

business_categories = []
tmp = []
for i in categories:
    tmp.append(i)
business_categories.append(tmp)
tmp = []
for i in categories:
    tmp.append("d")
business_categories.append(tmp)
business_categories.append([])
tmp = []

with open("../data/exp/edinburgh_business.json") as file:
    for line in file:
        js = json.loads(line)
        tmp = []
        for cat in categories:
            if cat in js["categories"]:
                tmp.append(1)
            else:
                tmp.append(0)
        business_categories.append(tmp)
    #do the attributes.

with open("../data/exp/edinburgh_reviews.json") as file:
    for line in file:
        js = json.loads(line)
        tmp = []
        tmp.append(js["business_id"])
        tmp.append(js["user_id"])
        date = js["date"].split('-')
        tmp.append(date[2])
        tmp.append(date[1])
        tmp.append(date[0])
        tmp.append(js["stars"])
        tmp.append(threshold(js["votes"]["funny"], tau))
        tmp.append(threshold(js["votes"]["useful"], tau))
        tmp.append(threshold(js["votes"]["cool"], tau))
        tmp.append(' '.join(js["text"].split()))
        reviews.append(tmp)

time = []
checkIns = []
# generate times
for i in range(7):
    for j in range(24):
        time.append(str(j) + "-" + str(i))
for i in range(7):
    time.append(i)
time.append("sum")
with open("../data/exp/edinburgh_checkins.json") as file:
    for line in file:
        js = json.loads(line)
        tmp = []
        tmp.append(js["business_id"])
        sum = 0
        for i in time[0:-8]:
            if i in js["checkin_info"].keys():
                tmp.append(js["checkin_info"][i])
                # sum += js["checkin_info"][i]
            else:
                tmp.append(0)
        # tmp.append(sum)
        checkIns.append(tmp)
    for i in range(len(checkIns)):
        sum = [0, 0, 0, 0, 0, 0, 0, 0]
        for j in range(len(checkIns[i]))[1:-8]:
            split = time[j].split('-')
            if(split[1] == '0' and checkIns[i][j] != '?'):
                sum[0] += int(checkIns[i][j])
            elif(split[1] == '1' and checkIns[i][j] != '?'):
                sum[1] += int(checkIns[i][j])
            elif(split[1] == '2' and checkIns[i][j] != '?'):
                sum[2] += int(checkIns[i][j])
            elif(split[1] == '3' and checkIns[i][j] != '?'):
                sum[3] += int(checkIns[i][j])
            elif(split[1] == '4' and checkIns[i][j] != '?'):
                sum[4] += int(checkIns[i][j])
            elif(split[1] == '5' and checkIns[i][j] != '?'):
                sum[5] += int(checkIns[i][j])
            elif(split[1] == '6' and checkIns[i][j] != '?'):
                sum[6] += int(checkIns[i][j])
            if (checkIns[i][j] != '?'):
                sum[7] += int(checkIns[i][j])
        # print len(checkIns[i])
        for j in sum:
            checkIns[i].append(j)
        # print len(checkIns[i])


with open("../data/exp/edinburgh_tips.json") as file:
    for line in file:
        js = json.loads(line)
        tmp = []
        tmp.append(js["business_id"])
        tmp.append(js["user_id"])
        date = js["date"].split('-')
        tmp.append(date[2])
        tmp.append(date[1])
        tmp.append(date[0])
        tmp.append(js["likes"])
        tmp.append(' '.join(js["text"].split()))
        tips.append(tmp)

printToTabFile("../data/tab/edinburgh_users.tab", users)
printToTabFile("../data/tab/edinburgh_business.tab", business)
printToTabFile("../data/tab/edinburgh_business_categories.tab", business_categories, categories)
printToTabFile("../data/tab/edinburgh_reviews.tab", reviews)
printToTabFile("../data/tab/edinburgh_checkins.tab", checkIns, time, False)
printToTabFile("../data/tab/edinburgh_tips.tab", tips)

printBusinesses("../data/tab/edinburgh_businesses_with_categories.tab", business, business_categories)