__author__ = 'pr3mar'
import numpy as np
import sys, random as rnd, time, datetime
import matplotlib.pyplot as plt
from scipy.stats import norm

def printMatrix(fname, matrix):
    sys.stdout = open(fname, "w")
    for i in matrix:
        for j in i:
            print j,
        print
    sys.stdout = sys.__stdout__
def pgdnmf(X, rank, max_iter=100, tol=1e-5, eta=1e-2):
    """
        NMF using projected gradient descent.
        Minimize || X - WH ||_F, subject to W, H >= 0.


        :param X:
            Data matrix.
        :param rank
            Maximal rank of the model (W, H).
        :param max_iter
            Maximum number of iterations.

        :return
            Data model W, H.
    """
    m, n  = X.shape
    W     = np.random.rand(m, rank)
    H     = np.random.rand(rank, n)
    itr   = 0
    error = float("inf")

    known = X.nonzero()

    while error > tol and itr < max_iter:
        error = 0
        if itr % 10 == 0:
            sys.stdout.write("%s/%s\r" % (itr, max_iter))
            sys.stdout.flush()

        dw = np.zeros((m, rank))
        dh = np.zeros((rank, n))
        for i, j in zip(*known):
            for k in xrange(rank):
                dw[i, k]    = dw[i, k] + (X[i, j] - W[i, :].dot(H[:, j])) * H[k, j]
                dh[k, j]    = dh[k, j] + (X[i, j] - W[i, :].dot(H[:, j])) * W[i, k]
            error += (X[i, j] - W[i, :].dot(H[:, j]))**2

        W = W + eta * dw
        H = H + eta * dh
        W = W * (W > 0)
        H = H * (H > 0)
        itr += 1

    return W, H
def nmf(Xs, rank, max_iter=100, allow_negative=False):
    """
    Standard non-negative matrix factorization.
    :param Xs
        Data matrices.
    :param rank
        Factorization rank (r).
    :param max_iter.
        Maximum number of iterations.
    :param getobj
        Return vector of cost function values.
    :return:
        W: A common row clustering matrix.
        Hs: A list of data-source specific matrices.
    """
    m, Ns = Xs[0].shape[0], [x.shape[1] for x in Xs]
    W = np.random.rand(m, rank)
    Hs = [np.random.rand(rank, n) for n in Ns]

    for itr in range(max_iter):
        sys.stderr.write("%d/%d\r" % (itr, max_iter) )
        sys.stderr.flush()

        enum = dotp([Xs[0], Hs[0].T])
        denom = dotp([W, dotp([Hs[0], Hs[0].T])])
        for i in range(1, len(Hs)):
            enum += dotp([Xs[i], Hs[i].T])
            denom += dotp([W, dotp([Hs[i], Hs[i].T])])
        W = np.nan_to_num(W * enum/denom)

        for i in range(len(Hs)):
            enum = dotp([W.T, Xs[i]])
            denom = dotp([W.T, W, Hs[i]])
            Hs[i] = np.nan_to_num(Hs[i] * enum/denom)

    return W, Hs

sample = False
date = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d-%H-%M-%S')
business = []
begin = time.time()
userBusiness = np.loadtxt("../data/user_business_matrix_normalized.txt")
tofloat = lambda x: (float(i) for i in x)

with open("../data/tab/business_experiment.tab") as file:
    i = 0
    for line in file:
        if i > 2:
            dataBusiness = line.split("\t")
            business.append(tofloat(dataBusiness[12:-1]))
        i += 1
busCat = np.zeros((len(business), 15))
for bus,i in zip(business,range(len(business))):
    for cat, j in zip(bus, range(15)):
        busCat[i, j] = cat
usrCat = userBusiness.dot(busCat)
indices = np.nonzero(usrCat)
if (sample):
    fname = "sampled_"
    k = int(round(float(len(indices[0]))*0.3))
    sample = rnd.sample(indices[0], k)
    samp = [[], []]
    for k in sample:
        samp[0].append(indices[0][k])
        samp[1].append(indices[1][k])
    samp[0] = np.array(samp[0])
    samp[1] = np.array(samp[1])
    sample = samp
    saveVals = usrCat[sample]
    usrCat[sample] = 0
    indices = sample
else:
    fname = ""
begNMF = time.time()
param1 = 10
param2 = 500
[W, H] = pgdnmf(usrCat, param1, param2)
print "time pgdnmf = ", time.time() - begNMF

approximationPGDNMF = W.dot(H)

aprValsPGDNMF = approximationPGDNMF[indices]
diffValsPGDNMF = (aprValsPGDNMF - usrCat[indices]) ** 2
print np.mean(diffValsPGDNMF), np.std(diffValsPGDNMF)

# printMatrix("../data/nmf/user-cat/" + fname + "bc-" + date + ".txt", busCat)
printMatrix("../data/nmf/user-cat/" + fname + "uc_" + str(param1) + "_" + str(param2) + "-" + date + ".txt", usrCat)
printMatrix("../data/nmf/user-cat/" + fname + "app_uc_" + str(param1) + "_" + str(param2) + "_" + date + ".txt", approximationPGDNMF)
printMatrix("../data/nmf/user-cat/" + fname + "w_" + str(param1) + "_" + str(param2) + "-" + date + ".txt", W)
printMatrix("../data/nmf/user-cat/" + fname + "h_" + str(param1) + "_" + str(param2) + "-" + date + ".txt", H)

indices = np.nonzero(usrCat)
diffPGDNMF = (approximationPGDNMF[indices] - usrCat[indices]) ** 2
# print "time = ", time.time() - begin


print "[pgdnmf] diff mean and std"
print np.mean(diffPGDNMF)
print np.std(diffPGDNMF)

x = np.linspace(-15,15,300)
orgNorm = norm.fit(usrCat[indices])
aprNorm = norm.fit(approximationPGDNMF[indices])
diffPDGNMFNorm = norm.fit(diffPGDNMF)

org_fitted = norm.pdf(x,loc=orgNorm[0],scale=orgNorm[1])
apr_fitted = norm.pdf(x,loc=aprNorm[0],scale=aprNorm[1])
diffPGDNMF_fitted = norm.pdf(x, loc=diffPDGNMFNorm[0], scale=diffPDGNMFNorm[1])

normal = norm.pdf(x)
plt.figure(figsize=plt.figaspect(0.5), dpi=100)
plt.title('user/cat')
plt.plot(x, normal, 'b-', label="normal distribution")
plt.plot(x, org_fitted, 'r-', label="original data")
plt.plot(x, apr_fitted, 'y-', label="approximated data")
plt.plot(x, diffPGDNMF_fitted, "g-", label="difference")
plt.legend()
plt.savefig("../data/imgs/ub_" + str(param1) + "_" + str(param2) + "_" + date + ".png", dpi=1000)
plt.show()